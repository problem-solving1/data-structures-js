const countingSort = (array) => {
  // declare a count array with 0 to maxIndex
  let count = [];

  // traverse actual array and store element count
  array.forEach((element) => {
    count[element] = count[element] === undefined ? 1 : (count[element] += 1);
  });

  // traverse count array and store (prev+current) as current
  let lastValue;
  count = count.map((element) => {
    lastValue = lastValue === undefined ? element : lastValue + element;
    return lastValue;
  });

  // create a new array to store sorted values
  let sortedArray = [];

  // traverse actual array and for each element, store in sorted array
  // at count[element] index
  array.forEach((element) => {
    let index = count[element] - 1;
    count[element]--;
    sortedArray[index] = element;
  });
};

// Time complexity O(N)
