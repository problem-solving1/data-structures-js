const merge = (array, leftSubArr, rightSubArr) => {
  let start1 = 0,
    start2 = 0;
  let arrIndex = 0;
  while (start1 < leftSubArr.length && start2 < rightSubArr.length) {
    if (leftSubArr[start1] <= rightSubArr[start2]) {
      array[arrIndex++] = leftSubArr[start1++];
    } else {
      array[arrIndex++] = rightSubArr[start2++];
    }
  }

  while (start2 < rightSubArr.length) {
    array[arrIndex++] = rightSubArr[start2++];
  }

  while (start1 < leftSubArr.length) {
    array[arrIndex++] = leftSubArr[start1++];
  }
};

const mergeSort = (array) => {
  debugger;
  if (array.length <= 1) return;

  let mid = Math.floor(array.length / 2);

  // create left and right copies
  let left = array.slice(0, mid);
  let right = array.slice(mid, array.length);

  // mergeSort on left sub-array
  mergeSort(left);

  // mergeSort on right sub-array
  mergeSort(right);

  merge(array, left, right);
};