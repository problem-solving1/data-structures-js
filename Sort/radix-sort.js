/**
 * Modified counting sort which creates an array by extracting digits from numbers and performs sort on the array
 *
 * @param {Number[]} array - the array to be sorted
 * @param {Number} place - 1st, 10th, 100th (etc.) position of a digit depending on which the array will be sorted
 */
const countingSort = (array, place) => {
  // for positive integers in the array
  let counts = [];
  let bucket = [];
  // for negative integers in the array
  let negCounts = [];
  let negBucket = [];

  // for every element, extract the place-th digit; keep count of the digits; push those numbers in a bucket whose indices are the digits
  array.forEach((number) => {
    let digit = Math.floor(Math.abs(number) / place) % 10;

    if (number >= 0) {
      // update count for each positive digit
      counts[digit] = !counts[digit] ? 1 : counts[digit] + 1;

      // place each positive number under the digit which was extracted from it
      if (bucket[digit]) {
        bucket[digit].push(number);
      } else {
        bucket[digit] = [number];
      }
    } else {
      // update count for each negative digit
      negCounts[digit] = !negCounts[digit] ? 1 : negCounts[digit] + 1;

      // place each negative number under the digit which was extracted from it
      if (negBucket[digit]) {
        negBucket[digit].push(number);
      } else {
        negBucket[digit] = [number];
      }
    }
  });

  // traverse counts array and store (prev+current) as current for positive numbers
  let lastValue;
  counts = counts.map((element) => {
    lastValue = lastValue === undefined ? element : lastValue + element;
    return lastValue;
  });

  lastValue = undefined;
  // traverse negCounts array and store (next+current) as current for positive numbers
  for (let index = negCounts.length - 1; index >= 0; index--) {
    if (negCounts[index] === undefined) continue;
    lastValue =
      lastValue === undefined ? negCounts[index] : lastValue + negCounts[index];
    negCounts[index] = lastValue;
  }

  // create a new array to store sorted values
  let sortedArray = [];

  /* traverse each digit extracted and place the origin numbers based on count value
   at count[element] index */
  // debugger;
  negCounts.forEach((_, digit) => {
    while (negBucket[digit].length) {
      let index = negCounts[digit] - 1;
      negCounts[digit]--;
      sortedArray[index] = negBucket[digit].pop();
    }
  });

  let prevLength = sortedArray.length;
  counts.forEach((_, digit) => {
    while (bucket[digit].length) {
      let index = counts[digit] - 1;
      counts[digit]--;
      sortedArray[prevLength + index] = bucket[digit].pop();
    }
  });

  return sortedArray;
};

/**
 * @param {Number[]} array - the array to be sorted
 */
const radixSort = (array) => {
  // do nothing when array is empty
  if (array.length === 0) return;

  // find the max number in array
  let max = Number.MIN_SAFE_INTEGER;
  array.forEach((number) => {
    max = number > max ? number : max;
  });

  // for every digit, create an array of digits and perform sort based on the digits
  let place = 1;
  while (max > 0) {
    array = countingSort(array, place);
    max = Math.floor(max / 10);
    place *= 10;
  }

  return array;
};

// Time complexity: O(N)
