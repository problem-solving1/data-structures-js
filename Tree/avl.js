const AVL = (function () {
  let weakMap = new WeakMap();

  const Node = function (data) {
    (this.data = data),
      (this.left = null),
      (this.right = null),
      (this.height = 0);
  };

  const AVL = function () {
    weakMap.set(this, null);
  };

  const getHeight = (node) => {
    if (!node) {
      return -1;
    } else {
      return node.height;
    }
  };

  const updateHeight = (node) => {
    if (!node.left && !node.right) node.height = 0;
    else {
      node.height =
        Math.max(node.left?.height ?? null, node.right?.height ?? null) + 1;
    }
  };

  const singleRotateLeft = function (node) {
    let newCentralNode = node.left;
    node.left = newCentralNode.right;
    newCentralNode.right = node;

    // newCentralNode.height =
    //   Math.max(
    //     0,
    //     newCentralNode.left?.height ?? null,
    //     newCentralNode.right?.height ?? null
    //   ) + 1;

    updateHeight(node);

    return newCentralNode;
  };

  const singleRotateRight = function (node) {
    let newCentralNode = node.right;
    node.right = newCentralNode.left;
    newCentralNode.left = node;

    // newCentralNode.height =
    //   Math.max(
    //     0,
    //     newCentralNode.left?.height ?? null,
    //     newCentralNode.right?.height ?? null
    //   ) + 1;

    updateHeight(node);

    return newCentralNode;
  };

  const doubleRotateLeft = function (node) {
    node.left = singleRotateRight(node.left);
    return singleRotateLeft(node);
  };

  const doubleRotateRight = function (node) {
    node.right = singleRotateLeft(node.right);
    return singleRotateRight(node);
  };

  const insertHelper = function (root, data) {
    if (!root) {
      root = new Node(data);
    } else if (data < root.data) {
      root.left = insertHelper(root.left, data);

      if (getHeight(root.left) - getHeight(root.right) === 2) {
        if (data < root.left.data) {
          root = singleRotateLeft(root);
        } else {
          root = doubleRotateLeft(root);
        }
      }
    } else if (data >= root.data) {
      root.right = insertHelper(root.right, data);

      if (getHeight(root.right) - getHeight(root.left) === 2) {
        if (data > root.right.data) {
          root = singleRotateRight(root);
        } else {
          root = doubleRotateRight(root);
        }
      }
    }

    updateHeight(root);

    return root;
  };

  AVL.prototype.insert = function (data) {
    let root = weakMap.get(this);
    root = insertHelper(root, data);
    weakMap.set(this, root);
  };

  // goes one node left and then all nodes right
  const findSuccessor = (root) => {
    let tempNode = root.left;
    while (tempNode.right) {
      tempNode = tempNode.right;
    }

    return tempNode;
  };

  const getBalanceFactor = (node) => {
    return getHeight(node.left) - getHeight(node.right);
  };

  const deleteHelper = function (root, data) {
    // navigating the node to delete
    if (!root) return null;
    else if (root.data === data) {
      if (root.left && root.right) {
        let successorNode = findSuccessor(root);
        root.data = successorNode.data;
        root.left = deleteHelper(root.left, successorNode.data);
      } else if (root.left) {
        root = root.left;
      } else if (root.right) {
        root = root.right;
      } else {
        root = null;
      }

      return root;
    } else if (data < root.data) {
      root.left = deleteHelper(root.left, data);
    } else {
      root.right = deleteHelper(root.right, data);
    }

    // checking node balance and updating balance
    if (getBalanceFactor(root) === 2) {
      if (getBalanceFactor(root.left) >= 0) {
        root = singleRotateLeft(root);
      } else {
        root = doubleRotateLeft(root);
      }
    } else if (getBalanceFactor(root) === -2) {
      if (getBalanceFactor(root.right) <= 0) {
        root = singleRotateRight(root);
      } else {
        root = doubleRotateRight(root);
      }
    }

    updateHeight(root);
    return root;
  };

  AVL.prototype.delete = function (data) {
    let root = weakMap.get(this);
    root = deleteHelper(root, data);
    weakMap.set(this, root);
  };

  AVL.prototype.print = function () {
    let root = weakMap.get(this);
    if (!root) return;

    let queue = [];

    queue.push(root);
    while (queue.length > 0) {
      let top = queue.shift();

      if (top.left) queue.push(top.left);
      if (top.right) queue.push(top.right);

      console.log(top.data);
    }
  };

  return AVL;
})();

export { AVL as default };
